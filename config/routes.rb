Rails.application.routes.draw do
  root 'static_pages#home'
  get :about_me, to: 'static_pages#about_me'
  get :contact, to: 'static_pages#contact'
end
